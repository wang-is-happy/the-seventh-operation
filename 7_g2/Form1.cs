using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;
namespace _7_g2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string s = textBox1.Text;
            //注：为了不使正则化表达式过于冗长和繁琐，特将平年和闰年出生的身份证号码的表达式分开。
            
            string pattern1 = @"^[1-9]\d{5}(\d{4})(((0[13578]|1[02])(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)(0[1-9]|[12][0-9]|30))|(02(0[1-9]|[1][0-9]|2[0-8])))\d{3}[0-9Xx]$";//平年
            string pattern2 = @"^[1-9]\d{5}(\d{2}(0[48]|[2468][048]|[13579][26])|((0[48]|1[26]|[2468][048]|[3579][26])00))0229\d{3}[0-9Xx]$";//闰年
           
            if (Regex.IsMatch(s,pattern2 )||Regex.IsMatch(s,pattern1)) 
            {
                MessageBox.Show("格式正确！", "查询结果", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("格式错误！", "查询结果", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int sum = 0;
            string s = textBox1.Text;
            string[] Wi = ("7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2").Split(',');//权重表
            string chCheckTable = ("10X98765432");//校验位



            for (int i = 0; i < Wi.Length; i++)
            {
                sum+=Convert.ToInt32(Wi[i])*Convert.ToInt32(s[i].ToString());
            }
            if(s[s.Length-1]==chCheckTable[sum%11])
            {
                MessageBox.Show("最后一位正确！", "查询结果", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("最后一位错误！"," 查询结果", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }
    }
}