using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;

public class Crawler
{
    private Hashtable urls = new Hashtable();

    private int count = 0;
    static void Main(string[] args)
    {
        Crawler myCrawler = new Crawler();

        string startUrl = "http://www.cnblogs.com/dstang2000/";
        if (args.Length >= 1) startUrl = args[0];
        myCrawler.urls.Add(startUrl, false);
        new Thread(myCrawler.Crawl).Start();
    }
    private void Crawl()
    {
        Console.WriteLine("开始爬行了....");

        while (true)
        {
            string current = null;
            foreach (string url in urls.Keys)
            {
                if ((bool)urls[url]) continue;
                current = url;
            }
            if (current == null || count > 10) break;
            Console.WriteLine("爬行" + current + "页面！");

            string html = DownLoad(current);
            urls[current] = true;
            count++;
            Parse(html,current);
        }
        Console.WriteLine("爬行结束！");

    }
    public string DownLoad(string url)
    {
        try
        {
            WebClient webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
            string html = webclient.DownloadString(url);

            string fileName = count.ToString();
            File.WriteAllText(fileName, html, Encoding.UTF8);
            return html;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return "";
        }

    }

    public void Parse(string html,string current)
    {
        string strRef = @"(href|HREF)[]*=[]*[""'][^""'#>]+[""']";
        MatchCollection matches = new Regex(strRef).Matches(html);
        foreach (Match match in matches)
        {
            strRef = match.Value.Substring(match.Value.IndexOf('=') + 1).Trim('"', '\'', '#', ' ', '>');
                string pattern= @"http.*";
            string pattern2 = @".*/";
            if(Regex.IsMatch(strRef, pattern))
            {
            }
            else if (Regex.IsMatch(current, pattern2))
            {
                strRef = current.Remove(current.Length) + strRef.Trim();
            }
            else
            {
                strRef=current+strRef.Trim();
            }

                if (strRef.Length == 0)
                continue;
            if (urls[strRef] == null)
                urls[strRef] = false;
        }


    }
}



