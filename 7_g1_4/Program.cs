﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;

public class Crawler
{
    internal Hashtable urls = new Hashtable();

    internal  int count = 0;
    internal string text=null;
    
    
    internal void Crawl()
    {
        text += "开始爬行了....\r\n";

        while (true)
        {
            string current = null;
            foreach (string url in urls.Keys)
            {
                if ((bool)urls[url]) continue;
                current = url;
            }
            if (current == null || count > 10) break;
            text += "爬行" + current + "页面！\r\n";

            string html = DownLoad(current);
            urls[current] = true;
            count++;
            Parse(html,current);
        }
        text += "爬行结束！";

    }
    public string DownLoad(string url)
    {
        try
        {
            WebClient webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
            string html = webclient.DownloadString(url);

            string fileName = count.ToString();
            File.WriteAllText(fileName, html, Encoding.UTF8);
            return html;
        }
        catch (Exception ex)
        {
            text += ex.Message + "\r\n";
            return "";
        }

    }

    public void Parse(string html,string current)
    {
        string strRef = @"(href|HREF)[]*=[]*[""'][^""'#>]+[""']";
        MatchCollection matches = new Regex(strRef).Matches(html);
        foreach (Match match in matches)
        {
            strRef = match.Value.Substring(match.Value.IndexOf('=') + 1).Trim('"', '\'', '#', ' ', '>');
                string pattern= @"http.*";
            string pattern2 = ".*//";
            if(Regex.IsMatch(strRef, pattern))
            {
            }
            else if (Regex.IsMatch(current, pattern2))
            {
                strRef = current.Remove(current.Length) + strRef.Trim();
            }
            else
            {
                strRef=current+strRef.Trim();
            }

                if (strRef.Length == 0)
                continue;
            if (urls[strRef] == null)
                urls[strRef] = false;
        }


    }
}



